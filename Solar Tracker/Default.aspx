﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" title="Solar Tracker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<P>Mijn naam is Rob Van Keilegom. Mijn GIP gaat over een zonnepaneel met zonnevolger. Deze GIP spreekt mij door het ongewone gebruik van de LED’s op de zonnevolger. Deze worden gebruikt om licht te detecteren en niet om licht uit te stralen.
Graag zou ik de volgende mensen willen bedanken voor de hulp aan mijn eindwerk:
Mr. L. Wielandt – voor de nodige begleiding en voor het maken van de zonnepaneel houder.
Mr. E. Marynissen – voor het ontwerpen en maken van de zonnepaneel stand.
</P>
</asp:Content>

