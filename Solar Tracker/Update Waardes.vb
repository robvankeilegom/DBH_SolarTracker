﻿Imports System.IO.Ports
Imports System.Text
Module Update_Waardes
    Public Function Update(ByVal Gegevens As Byte) As String
        Dim Com3 As New IO.Ports.SerialPort
        With Com3
            .PortName = "COM3"
            .BaudRate = 9600
            .DataBits = 8
            .StopBits = StopBits.One
            .Parity = Parity.None
            '.ReadBufferSize = 32768 '32768=size of buffer in bytes
            .ReadTimeout = 5000 '=5sec
            '.Handshake = Handshake.RequestToSend 'works when set to none
            .DtrEnable = True 'tried both true/false
            .RtsEnable = True 'tried both true/false
            '.Encoding = Encoding.Default
            .NewLine = Chr(13) '13=carriage return
            '.ReceivedBytesThreshold = 1
        End With

        Com3.Open()

        Com3.WriteLine(Gegevens.ToString & vbNewLine)

        Do
            Try
                Dim Incoming As String = Com3.ReadLine()
                If Incoming = Nothing Then

                Else
                    Com3.Close()
                    Return Incoming

                    Exit Do

                End If
            Catch ex As Exception

            End Try
            
        Loop

        Com3.Close()

    End Function
End Module
