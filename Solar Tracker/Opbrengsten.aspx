﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Opbrengsten.aspx.vb" Inherits="Opbrengsten" title="Solar Tracker" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="box">
    <!--
    <asp:Label ID="Label1" runat="server" Text="Spanning: " Height="20px"></asp:Label>
    <br />
    <asp:Label ID="Label2" runat="server" Text="Stroom: " Height="20px"></asp:Label>
    <br />
    <asp:Label ID="Label3" runat="server" Text="Vermogen: " Height="20px"></asp:Label>
    <br />
    -->
    <asp:Label ID="Label7" runat="server" Text="Verhouding LED's (Horizontaal): " Height="20px"></asp:Label>
    <br />
    <asp:Label ID="Label8" runat="server" Text="Verhouding LED's (Verticaal): " Height="20px"></asp:Label>
    <br />
    <asp:Label ID="Label11" runat="server" Text="Stand Servo: " Height="20px"></asp:Label>
    <br />
    <asp:Label ID="Label12" runat="server" Text="Stand Stepper: " Height="20px"></asp:Label>
    <br />
    </div>
    <div class="box">
    <!--
    <asp:TextBox ID="txtSpanning" runat="server" ReadOnly="True"></asp:TextBox>
    <br />
     <asp:TextBox ID="txtStroom" runat="server" ReadOnly="True"></asp:TextBox>
    <br />
     <asp:TextBox ID="txtVermogen" runat="server" ReadOnly="True"></asp:TextBox>
    <br />
    -->
     <asp:TextBox ID="txtVerhLedsHor" runat="server" ReadOnly="True"></asp:TextBox>
    <br />
     <asp:TextBox ID="txtVerhLedsVer" runat="server" ReadOnly="True"></asp:TextBox>
    <br />
     <asp:TextBox ID="txtStandServo" runat="server" ReadOnly="True"></asp:TextBox>
    <br />
     <asp:TextBox ID="txtStandStepper" runat="server" ReadOnly="True"></asp:TextBox>
    <br />
    </div>
        <div class="box">
    <!--
    <asp:Label ID="Label4" runat="server" Text="(V)" Height="20px"></asp:Label>
    <br />
    <asp:Label ID="Label5" runat="server" Text="(A)" Height="20px"></asp:Label>
    <br />
    <asp:Label ID="Label6" runat="server" Text="(W)" Height="20px"></asp:Label>
    <br />
    -->
    <asp:Label ID="Label9" runat="server" Text="(V)" Height="20px"></asp:Label>
    <br />
    <asp:Label ID="Label10" runat="server" Text="(V)" Height="20px"></asp:Label>
    <br />
    <asp:Label ID="Label13" runat="server" Text="°" Height="20px"></asp:Label>
    <br />
    <asp:Label ID="Label14" runat="server" Text="°" Height="20px"></asp:Label>
    <br />
     <br />
    </div>
    <br />
    <asp:Label ID="Label15" runat="server" Text="Probleem? Probeer de pagina te herladen." Height="28px"></asp:Label> 
    <asp:Button ID="Button1" runat="server" Text="Herladen" Width="102px" />
    <br />
</asp:Content>

